/*
 * @Descripttion:
 * @Author: qinyonglian
 * @Date: 2019-11-11 15:39:57
 * @LastEditors: qinyonglian
 * @LastEditTime: 2019-12-02 16:09:43
 */

// 回收站列表
import RecoveryModel from '../../model/recoveryModel';
import {
  queryRecycleBin,
  recoveryRecycleBin,
  emptyRecycleBin,
} from '../../actions/action';

export default class RecoveryList {
  /* 回收站列表
   * @param {key} 搜索关键字
   * @param {order} 排序条件[{code:"name||size",type:"desc||asc"}]
   * @param {disktype}
   * @return:
   */
  async recoveryList(key, order) {
    // 1.调取接口，渲染列表
    // 2.数据模型 {
    const res = await queryRecycleBin({
      key,
      order: JSON.stringify(order),
    });
    return res.map(item => RecoveryModel.create(item));
  }

  /* 清除回收站
   * @param {recycleId} 删除记录的id
   * @return:
   */
  async emptyRecycle(recycleId) {
    const res = await emptyRecycleBin({
      recycleIds: recycleId.join(','),
    });
    return res;
  }

  /* 恢复回收站
   * @param {recycleId} 恢复文件的id
   * @return:
   */
  async recoveryFile(recycleIds, diskType) {
    const res = await recoveryRecycleBin({
      recycleIds,
      diskType,
    });
    return res;
  }

  static create() {
    return new RecoveryList(...arguments);
  }
}
