/**
 * 庞峰  流程发起  相关接口
 */
// 服务器地址
import { HOST_RBAC, HOST_BASIC } from '../../../../../constants/config';
// 查询流程发起的ico数据
export const WORK_START_LISTDATA = `${HOST_BASIC}/je/menu/menu/loadWfModule`;