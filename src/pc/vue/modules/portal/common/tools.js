
// 还需要时间处理工具
/**
 * 处理图片
 */
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import 'dayjs/locale/zh-cn';

dayjs.locale('zh-cn');
// 设置时间
dayjs.extend(relativeTime);
export function getImg(single, data, lb) {
  // 返回图片数组或者图片地址
  if (single) {
    return `${JE.buildDocUrl()}?fileKey=${data.split('*')[1]}`;
  }
  if (data && Array.isArray(JSON.parse(data))) {
    const imgArry = [];
    JSON.parse(data).map((imgItem, index) => {
      if (index > 4) return;
      imgArry.push(`${JE.buildDocUrl(lb ? 'thumbnail' : 'preview')}?fileKey=${imgItem.path}`);
    });
    return imgArry;
  }
}
/**
 *处理时间
 */
export function dateFormat4Time(date, time) {
  if (JE.isEmpty(date)) { return ''; }

  const today = dayjs();
  const formatDate = dayjs(date);
  const curYear = dayjs().year();
  const curMonth = dayjs().month() + 1;
  const curDay = dayjs().date();
  const curDate = `${curYear}-${curMonth}-${curDay}`;
  const time24 = dayjs(`${curDate} 00:00`);
  const diff1 = formatDate.diff(today, 'day'); // 相差天数
  const diff2 = time24.diff(today, 'hour'); // 今天的相差小时数
  const diff3 = formatDate.diff(today, 'hour');// 相差小时数
  const week = today.day();// 星期

  let msg = '';
  const absNum = Math.abs(diff1);
  const absHour = Math.abs(diff2); // 当天从零点开始到目前的小时数
  const absrelHour = Math.abs(diff3); // 相差小时

  if (time) {
    if (absrelHour <= absHour) { // 当天的日期显示 时：分
      msg = '今天';// formatDate.format('HH:mm');
    } else if (absrelHour > absHour && (absrelHour - absHour) < 24) { // 昨天的日期显示昨天
      msg = `昨天${formatDate.format('HH:mm')}`;
    } else if (week - absNum > 0) { // 本周的日期显示本周
      msg = `${formatDate.format('dddd')} ${formatDate.format('HH:mm')}`;
    } else if (formatDate.format('YYYY') == today.format('YYYY')) { // 本年的显示月和日
      msg = '本月';// `${formatDate.format('MM-DD')} ${formatDate.format('HH:mm')}`;
    } else { // 完整时间
      msg = `${formatDate.format('YYYY-MM-DD')} ${formatDate.format('HH:mm')}`;
    }
  }
  if (!time) {
    if (absrelHour < absHour) { // 当天的日期显示 时：分
      msg = '今天';// formatDate.format('HH:mm');
    } else if (absrelHour > absHour && (absrelHour - absHour) < 24) { // 昨天的日期显示昨天
      msg = '昨天';
    } else if (week - absNum > 0) { // 本周的日期显示本周
      msg = formatDate.format('dddd');
    } else if (formatDate.format('YYYY') == today.format('YYYY')) { // 本年的显示月和日
      msg = '本月';// formatDate.format('MM-DD');
    } else { // 完整时间
      msg = formatDate.format('YYYY-MM-DD');
    }
  }
  // 列表：
  // 当天：时：分
  // 昨天：昨天，时：分
  // 本周：周几，时：分
  // 当前：月 - 日，时：分
  // 更早：年 - 月 - 日，时：分
  return msg;
}


export function isShowTime(date) {
  if (JE.isEmpty(date)) { return ''; }
  const formatDate = dayjs(date).valueOf();
  // 当前的年月日
  const curYear = dayjs().year();
  const curMonth = dayjs().month();
  // 获取本月的初始时间戳
  const curMonthTime = new Date(curYear, curMonth, 1).getTime();
  // 穿过来的时间2019-10-11，咱们变为时间戳、如果这个时间戳大于等于curMonthTime，返回true，否则false
  if (formatDate >= curMonthTime) {
    return true;
  }
  return false;
}

export function setListTime(date) {
  return `${date.substr(0, 13).replace('-', '年').replace('-', '月')}点`;
  // if (JE.isEmpty(date)) { return ''; }
  // const formatDate = dayjs(date).valueOf();
  // // 当前的年月日
  // const curYear = dayjs().year();
  // const curMonth = dayjs().month();
  // // 获取本月的初始时间戳
  // const curMonthTime = new Date(curYear, curMonth, 1).getTime();
  // // 穿过来的时间2019-10-11，咱们变为时间戳、如果这个时间戳大于等于curMonthTime，返回true，否则false
  // if (formatDate >= curMonthTime) {
  //   return true;
  // }
  // return false;
}
