/*
 * @Author: megnping Zhang
 * @Date: 2019-10-14
 * @LastEditors: megnping Zhang
 * @LastEditTime: 2019-10-14
/* 常量
 * @Author: megnping Zhang
 * @Date: 2019-10-14
 */
export const COMMON = {
  MHID: 'MHID', // 门户id
  SEARCHTAB: 'SEARCHTAB',
  SEARCHLIST: 'SEARCHLIST',
  NEWSDEATIL: 'NEWSDEATIL',
  CLOSEPAGE: 'CLOSEPAGE',
  BACKFROMDETAIL: 'BACKFROMDETAIL', // 从详情页面返回来的
  SCROLLLOAD: 'SCROLLLOAD',
  SCROLHIDE: 'SCROLHIDE',
};
