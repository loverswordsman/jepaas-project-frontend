/*
 * @Descripttion:
 * @Author: qinyonglian
 * @Date: 2019-11-19 16:29:18
 * @LastEditors: qinyonglian
 * @LastEditTime: 2020-03-11 16:32:30
 */

import List from '../controllers/list/listController';
import AcceptList from '../controllers/list/acceptController';
import RecordList from '../controllers/list/recordController';
import RecoveryList from '../controllers/list/recoveryController';
import { getAssignKeyData, getCheckArr } from './util';

const objList = new List();
const acceptList = new AcceptList();
const recordList = new RecordList();
const recoveryList = new RecoveryList();
/**
 * 删除公共方法进入，根据参数的不同调用不同的接口
 *
 * @export
 * @param {String} nodeIds 当前需要删除的nodeIds
 * @param {Onject} menuData 当前需要删除的菜单数据
 * @param {Array} mainList 当前列表的所有数据
 */
// eslint-disable-next-line import/prefer-default-export
export async function removeUtilt(nodeIds, menuData, mainList) {
  let res;
  let key;
  // console.log('nodeIds', nodeIds);
  if (menuData.router == '1-1') {
    key = 'nodeId';
    res = await objList.fileByRecycle(nodeIds, 'company');
  } else if (menuData.router == '1-2') {
    key = 'nodeId';
    res = await objList.fileByRecycle(nodeIds, 'self');
  } else if (menuData.router == '1-3') {
    key = 'id';
    res = await acceptList.shareDeleteList(nodeIds);
  } else if (menuData.router == '1-4') {
    key = 'id';
    res = await acceptList.shareDeleteList(nodeIds);
  } else if (menuData.router == '2') {
    key = 'UUID';
    // console.log('当前的菜单2', menuData);
  } else if (menuData.router == '3') {
    key = 'id';
    res = await recordList.deleteRecord(nodeIds);
  } else if (menuData.router == '5') {
    key = 'id';
    res = await recoveryList.emptyRecycle(nodeIds);
  }
  // 上面就是接口调取完成之后下方执行的方法
  // nodeId 需要遍历的key, nodeIds 数组数据, mainList 所有需要遍历的数据
  // 这边的res成功的话就返回true, 失败就返回false
  if (!res) return false;
  const mainData = getAssignKeyData(key, nodeIds, mainList);
  if (mainData.length) {
    return getCheckArr(false, mainData, 'checkbox');
  }
  // 已经去除掉的指定遍历的数据 mainData
  return [];
}
