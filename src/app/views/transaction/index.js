
import router from './router.js';
import config from './config.json';
import install from '../../util/install.js';

install(router, config);
