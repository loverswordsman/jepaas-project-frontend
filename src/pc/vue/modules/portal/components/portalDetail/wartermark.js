// *水印
/**
*参数： {
    str： 第一行字体
    str2： 第二行字体
    parent： 水印放入的父元素id
}
*/
const watermark = {};

const setWatermark = (str, str2, parent, handler) => {
  const id = '1.23452384164.123412415';
  if (document.getElementById(id) !== null) {
    document.getElementById(parent).removeChild(document.getElementById(id));
  }

  // if (handler == 'add') {
  const can = document.createElement('canvas');
  can.width = 190;
  can.height = 115;

  const cans = can.getContext('2d');
  cans.rotate(-15 * Math.PI / 180);
  cans.font = '15px Vedana';
  cans.fillStyle = 'rgba(150, 150, 150, 0.50)';
  cans.textAlign = 'center';
  cans.textBaseline = 'Middle';
  // cans.fillText(str, can.width / 3, can.height / 2);
  cans.fillText(str, 50, 50); // 第一行字体
  cans.fillText(str2, 50, 65); // 第二行字体

  const div = document.createElement('div');
  div.id = id;
  div.style.pointerEvents = 'none';
  div.style.top = '0';
  div.style.left = '0';
  div.style.position = 'absolute';
  div.style.zIndex = '100000';
  div.style.width = '100%';
  div.style.height = '100%';
  div.style.background = `url(${can.toDataURL('image/png')}) left top repeat`;
  /* // 如果传ID  就放到这个id元素内   若不传  就放到body中  全屏展示
  if (document.getElementById(parent)) {
    document.getElementById(parent).appendChild(div);
  } else {
    document.body.appendChild(div);
  } */
  document.getElementById(parent) && document.getElementById(parent).appendChild(div);

  return id;
  // }
};

// 该方法只允许调用一次
watermark.set = (str, str2, parent) => {
  let id = setWatermark(str, str2, parent);
  const dsq = setInterval(() => {
    if (document.getElementById(id) === null) {
      id = setWatermark(str, str2, parent);
      clearInterval(dsq);
    }
  }, 500);
  /* window.onresize = () => {
    setWatermark(str, str2, parent);
  }; */
};

export default watermark;
